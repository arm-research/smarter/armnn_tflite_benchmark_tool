# ArmNN TFLite Benchmarking Tool

This tool is a convenient way to compare performance of TFLite models using different versions of TFLite and ArmNN

It also is designed to compare the measured performance using the tflite benchmarking tool versus the benchmarking tool for Nvidia triton with the ArmNN TFLite backend

## Installation
To setup your environment to run this tool, please run the following to setup your host
```bash
sudo apt update && sudo apt install -y python3-pip
python3 -m pip install docker jinja2 requests

# Install TFLite python3 package
echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | sudo tee /etc/apt/sources.list.d/coral-edgetpu.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update
sudo apt-get install python3-tflite-runtime
```

Login to **github** container registry using a generated token. To login run:
```bash
echo <your github token> | docker login https://ghcr.io -u <github username> --password-stdin
```

or if your token is in a file:
```bash
cat <your token file> | docker login https://ghcr.io -u <github username> --password-stdin
```

Login to **gitlab** container registry using a generated token. 
First generate a token with read registry permissions by following the instructions [here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token).
Then to login run:
```bash
docker login registry.gitlab.com -u <gitlab username> -p <gitlab token>
``` 

## Usage
Run TFLite benchmark:
```bash
python3 benchmark_model.py --model <Model Path> --tflite-version v2.4.1 --armnn-version v21.05 v21.08 --ruy ON OFF -b tflite 
```

Run Triton benchmark with TFLite backend:
```
python3 benchmark_model.py --model <Model Path> --armnn-version v21.05 v21.08 --ruy ON OFF -b triton 
```

## Notes
- By default the number of threads used by the runtime will increment in powers of 2 up to the max hw thread count for the system
- Latency measurement outputs are in us and memory measurement outputs are in MB