from abc import ABC, abstractmethod
import docker


class Benchmark(ABC):
    def __init__(
        self,
        model_path,
        num_threads,
        armnn_version,
        ruy,
        build_type,
        use_fast_math,
    ):
        self.model_path = model_path
        self.num_threads = num_threads
        self.armnn_version = armnn_version
        self.ruy = ruy
        self.build_type = build_type
        self.use_fast_math = use_fast_math
        self.client = docker.from_env(timeout=3600)

    @abstractmethod
    def run(self):
        pass
