import argparse
import itertools
from collections import defaultdict
import multiprocessing
from tflite_benchmark import TFLiteBenchmark
from triton_benchmark import TritonBenchmark
import math
import requests
import logging
from enum import IntEnum
import csv
import os


class TestParams(IntEnum):
    BENCHMARK_TYPE = 0
    ARMNN_VERSION = 1
    RUY = 2
    BUILD_TYPE = 3
    NUM_THREADS = 4
    TFLITE_VERSION = 5
    ARMNN_TFLITE_BACKEND_VERSION = 6
    TFLITE_RUNTIME = 7
    TRITON_VERSION = 8
    CPU_INSTANCES = 9
    GPU_INSTANCES = 10
    FAST_MATH = 11


def add_row_to_csv_file(filename, param_set, result_dict):
    file_exists = os.path.isfile(filename)

    with open(filename, "a") as csvfile:
        headers = [
            "BenchmarkType",
            "ArmNNVersion",
            "RUY",
            "BuildType",
            "NumThreads",
            "TFLiteVersion",
            "ArmNNTFLiteBackendVersion" "TFLiteRuntime",
            "CPUInstances",
            "GPUInstances",
            "FastMath",
            "Memory",
            "ComputeLatency",
            "ClientLatency",
            "Throughput",
        ]
        writer = csv.DictWriter(
            csvfile, delimiter=",", lineterminator="\n", fieldnames=headers
        )

        if not file_exists:
            writer.writeheader()  # file doesn't exist yet, write a header

        row = {
            "BenchmarkType": param_set[TestParams.BENCHMARK_TYPE],
            "ArmNNVersion": param_set[TestParams.ARMNN_VERSION],
            "RUY": param_set[TestParams.RUY],
            "BuildType": param_set[TestParams.BUILD_TYPE],
            "NumThreads": param_set[TestParams.NUM_THREADS],
            "TFLiteVersion": param_set[TestParams.TFLITE_VERSION],
            "ArmNNTFLiteBackendVersion": param_set[
                TestParams.ARMNN_TFLITE_BACKEND_VERSION
            ],
            "TFLiteRuntime": param_set[TestParams.TFLITE_RUNTIME],
            "CPUInstances": param_set[TestParams.CPU_INSTANCES],
            "GPUInstances": param_set[TestParams.GPU_INSTANCES],
            "FastMath": param_set[TestParams.FAST_MATH],
            "ComputeLatency": result_dict["compute_latency"],
        }

        if param_set[TestParams.BENCHMARK_TYPE] == "tflite":
            row["Memory"] = result_dict["memory"]
            row["ClientLatency"] = None
            row["Throughput"] = None
        elif param_set[TestParams.BENCHMARK_TYPE] == "triton":
            row["ClientLatency"] = result_dict["client_latency"]
            row["Throughput"] = result_dict["throughput"]
            row["Memory"] = None

        writer.writerow(row)


def benchmark_tflite(param_set, result_set):
    key = "armnn_{}_ruy_{}_tflite_{}_runtime_{}_threads_{}_fastmath_{}_gpu_{}_buildtype_{}".format(
        param_set[TestParams.ARMNN_VERSION]
        if param_set[TestParams.ARMNN_VERSION]
        else "None",
        param_set[TestParams.RUY],
        param_set[TestParams.TFLITE_VERSION],
        param_set[TestParams.TFLITE_RUNTIME],
        param_set[TestParams.NUM_THREADS],
        param_set[TestParams.FAST_MATH],
        ("ON" if param_set[TestParams.GPU_INSTANCES] else "OFF"),
        param_set[TestParams.BUILD_TYPE],
    )

    # Run benchmark if param_set untested and cpu or gpu instances is 1 (mutually exclusive)
    if not (key in result_set["tflite"]):
        logging.info("TFLite Benchmark w/ params {}".format(key))
        image_tag = "armnn_{}_tflite_{}_ruy_{}_{}".format(
            param_set[TestParams.ARMNN_VERSION]
            if param_set[TestParams.ARMNN_VERSION]
            else "v21.05",
            param_set[TestParams.TFLITE_VERSION],
            param_set[TestParams.RUY],
            param_set[TestParams.BUILD_TYPE],
        ).lower()
        if args.armnn_registry:
            armnn_tflite_image = "{}:{}".format(args.armnn_registry, image_tag)
        else:
            armnn_tflite_image = "registry.gitlab.com/jishminor/armnn_docker:{}".format(
                image_tag
            )
        logging.debug("ArmNN Docker image: {}".format(armnn_tflite_image))

        result = TFLiteBenchmark(
            model_path=args.model,
            num_threads=param_set[TestParams.NUM_THREADS],
            armnn_version=param_set[TestParams.ARMNN_VERSION],
            ruy=param_set[TestParams.RUY],
            use_fast_math=(True if param_set[TestParams.FAST_MATH] == "ON" else False),
            build_type=param_set[TestParams.BUILD_TYPE],
            armnn_tflite_image=armnn_tflite_image,
            tflite_version=param_set[TestParams.TFLITE_VERSION],
            runtime=param_set[TestParams.TFLITE_RUNTIME],
            use_gpu=(True if param_set[TestParams.GPU_INSTANCES] else False),
        ).run()

        result_set["tflite"][key] = result

        logging.info(dict(result))

        return result


def benchmark_triton(param_set, result_set):
    # Run benchmark if param_set untested
    key = "armnn_{}_ruy_{}_tflite_{}_runtime_{}_threads_{}_fastmath_{}_cpu_{}_gpu_{}_backendver_{}_buildtype_{}".format(
        param_set[TestParams.ARMNN_VERSION]
        if param_set[TestParams.ARMNN_VERSION]
        else "None",
        param_set[TestParams.RUY],
        param_set[TestParams.TFLITE_VERSION],
        param_set[TestParams.TFLITE_RUNTIME],
        param_set[TestParams.NUM_THREADS],
        param_set[TestParams.FAST_MATH],
        param_set[TestParams.CPU_INSTANCES],
        param_set[TestParams.GPU_INSTANCES],
        param_set[TestParams.ARMNN_TFLITE_BACKEND_VERSION],
        param_set[TestParams.BUILD_TYPE],
    )
    if not (key in result_set["triton"]):
        # Set triton image
        triton_image_tag = "{}_armnn_{}_ruy_{}_gpu_{}_backendver_{}_{}".format(
            param_set[TestParams.TRITON_VERSION],
            param_set[TestParams.ARMNN_VERSION]
            if param_set[TestParams.ARMNN_VERSION]
            else "v21.05",
            param_set[TestParams.RUY],
            ("on" if param_set[TestParams.GPU_INSTANCES] > 0 else "off"),
            param_set[TestParams.ARMNN_TFLITE_BACKEND_VERSION],
            param_set[TestParams.BUILD_TYPE],
        )
        if args.triton_registry:
            triton_image = "{}:{}".format(args.triton_registry, triton_image_tag)
        else:
            triton_image = "ghcr.io/jishminor/server/tritonserver:{}".format(
                triton_image_tag
            )

        logging.info("Triton Benchmark w/ params {}".format(key))
        logging.debug("Triton image: {}".format(triton_image))

        # Set perf_analyzer image
        perf_analyzer_image_tag = "{}_arm64".format(
            param_set[TestParams.TRITON_VERSION]
        )
        if args.perf_analyzer_registry:
            perf_analyzer_image = "{}:{}".format(
                args.perf_analyzer_registry,
                perf_analyzer_image_tag,
            )
        else:
            perf_analyzer_image = (
                "registry.gitlab.com/jishminor/client/perf_analyzer:{}".format(
                    perf_analyzer_image_tag
                )
            )

        result = TritonBenchmark(
            model_path=args.model,
            num_threads=param_set[TestParams.NUM_THREADS],
            armnn_version=param_set[TestParams.ARMNN_VERSION],
            ruy=param_set[TestParams.RUY],
            use_fast_math=(True if param_set[TestParams.FAST_MATH] == "ON" else False),
            build_type=param_set[TestParams.BUILD_TYPE],
            triton_image=triton_image,
            perf_analyzer_image=perf_analyzer_image,
            runtime=param_set[TestParams.TFLITE_RUNTIME],
            cpu_instances=param_set[TestParams.CPU_INSTANCES],
            gpu_instances=param_set[TestParams.GPU_INSTANCES],
        ).run()

        result_set["triton"][key] = result

        logging.info(dict(result))

        return result


if __name__ == "__main__":
    # Compute default thread counts for test
    cpus = multiprocessing.cpu_count()
    thread_max_log = math.log2(cpus)
    thread_count = [
        2 ** j
        for j in range(
            0,
            int(thread_max_log + (1 if thread_max_log.is_integer() else 2)),
        )
    ]
    if thread_count[-1] > cpus:
        thread_count[-1] = cpus

    # Gather cli args
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-d",
        "--debug",
        help="Print lots of debugging statements",
        action="store_const",
        dest="loglevel",
        const=logging.DEBUG,
        default=logging.INFO,
    )
    parser.add_argument(
        "-q",
        "--quiet",
        help="Be quiet",
        action="store_const",
        dest="loglevel",
        const=logging.WARNING,
    )
    parser.add_argument(
        "-o",
        "--output-file",
        type=str,
        required=False,
        help="Output file to write csv file with results to",
    )
    parser.add_argument(
        "--armnn-registry",
        type=str,
        required=False,
        help="Url of registry to source armnn docker images from",
    )
    parser.add_argument(
        "--triton-registry",
        type=str,
        required=False,
        help="Url of registry to source triton docker images from",
    )
    parser.add_argument(
        "--perf-analyzer-registry",
        type=str,
        required=False,
        help="Url of registry to source perf_analyzer docker images from",
    )
    parser.add_argument(
        "--model",
        type=str,
        required=True,
        help="File path to tflite model",
    )
    parser.add_argument(
        "-b",
        "--benchmark-types",
        type=str,
        required=False,
        default=["tflite", "triton"],
        choices=["tflite", "triton"],
        nargs="+",
        help="Tool to use to output benchmark results",
    )
    parser.add_argument(
        "--triton-refs",
        type=str,
        required=False,
        default=["main"],
        choices=["main"],
        nargs="+",
        help="Triton ref associated with the build.",
    )
    parser.add_argument(
        "--armnn-tflite-backend-refs",
        type=str,
        required=False,
        default=["main"],
        nargs="+",
        help="ArmNN TFLite backend refs associated with the benchmarks.",
    )
    parser.add_argument(
        "--triton-protocols",
        type=str,
        required=False,
        default=["http", "grpc"],
        choices=["http", "grpc"],
        nargs="+",
        help="Triton client protocols to use in benchmark.",
    )
    parser.add_argument(
        "--armnn-versions",
        type=str,
        required=False,
        default=["v21.05", "v21.08"],
        choices=["v21.05", "v21.08"],
        nargs="+",
        help="ArmNN tag associated with the build.",
    )
    parser.add_argument(
        "--tflite-versions",
        type=str,
        required=False,
        default=["v2.3.1", "v2.4.1"],
        choices=["v2.3.1", "v2.4.1"],
        nargs="+",
        help="TFLite tag associated with the build.",
    )
    parser.add_argument(
        "--ruy",
        type=str,
        required=False,
        default=["ON", "OFF"],
        choices=["ON", "OFF"],
        nargs="+",
        help="Whether to build ruy support into tflite",
    )
    parser.add_argument(
        "--fast-math",
        type=str,
        required=False,
        default=["ON", "OFF"],
        choices=["ON", "OFF"],
        nargs="+",
        help="Whether to enable use of fast math",
    )
    parser.add_argument(
        "--build-types",
        type=str,
        required=False,
        default=["RELEASE"],
        choices=["RELEASE", "DEBUG"],
        nargs="+",
        help="Build types to benchmark",
    )
    parser.add_argument(
        "--num-threads",
        type=int,
        required=False,
        default=thread_count,
        nargs="+",
        help="Number of threads to test. By default starts at 1 and scales by powers of 2 until machine cpu count",
    )
    parser.add_argument(
        "--tflite-runtimes",
        type=str,
        required=False,
        default=["default", "xnnpack", "armnn"],
        choices=["default", "xnnpack", "armnn"],
        nargs="+",
        help="TFLite runtimes associated with the build",
    )
    parser.add_argument(
        "--max-cpu-instances",
        type=int,
        required=False,
        default=1,
        help="Number of cpu instances to test with",
    )
    parser.add_argument(
        "--max-gpu-instances",
        type=int,
        required=False,
        default=0,
        help="Number of gpu instances to test with",
    )
    args = parser.parse_args()
    logging.basicConfig(level=args.loglevel)

    # Add None to ArmNN versions and Fast Math for benchmarks where it is irrelevant
    args.armnn_versions.append(None)
    args.fast_math.append(None)

    # Get Triton version corresponding to each triton ref
    triton_versions = []
    for triton_ref in args.triton_refs:
        # Set triton version using information from repository
        try:
            triton_versions.append(
                requests.get(
                    "https://raw.githubusercontent.com/triton-inference-server/server/{}/TRITON_VERSION".format(
                        triton_ref
                    )
                ).text.replace("\n", "")
            )
        except Exception as e:
            logging.info(
                "Failed to fetch triton version using passed triton ref with error: {}".format(
                    e
                )
            )

    # Generate cartesian product of all the options passed for the test
    test_params = [
        x
        for x in itertools.product(
            *[
                args.benchmark_types,
                args.armnn_versions,
                args.ruy,
                args.build_types,
                args.num_threads,
                args.tflite_versions,
                args.armnn_tflite_backend_refs,
                args.tflite_runtimes,
                triton_versions,
                range(0, args.max_cpu_instances + 1),
                range(0, args.max_gpu_instances + 1),
                args.fast_math,
            ]
        )
    ]

    # Remove all test params where both cpu and gpu instances are non-zero or both are zero
    test_params = [
        x
        for x in test_params
        if bool(x[TestParams.CPU_INSTANCES] > 0) ^ bool(x[TestParams.GPU_INSTANCES] > 0)
    ]

    # Remove all test params where cpu and gpu instances are greater than 1 for all tests
    test_params = [
        x
        for x in test_params
        if not (
            x[TestParams.TFLITE_RUNTIME] != "armnn" and x[TestParams.GPU_INSTANCES] > 0
        )
    ]

    # Remove tests where gpu instances greater than one and runtime is not armnn
    test_params = [
        x
        for x in test_params
        if not (
            x[TestParams.BENCHMARK_TYPE] == "tflite"
            and (x[TestParams.CPU_INSTANCES] > 1 or x[TestParams.GPU_INSTANCES] > 1)
        )
    ]

    # Remove all triton tests where tflite version is v2.3.1 as there doesn't exist a build
    test_params = [
        x
        for x in test_params
        if not (
            x[TestParams.BENCHMARK_TYPE] == "triton"
            and (x[TestParams.TFLITE_VERSION] == "v2.3.1")
        )
    ]

    # Remove all tests where ArmNN version is set and runtime is not ArmNN
    test_params = [
        x
        for x in test_params
        if not (
            (x[TestParams.ARMNN_VERSION] != None or x[TestParams.FAST_MATH] != None)
            and (
                x[TestParams.TFLITE_RUNTIME] == "default"
                or x[TestParams.TFLITE_RUNTIME] == "xnnpack"
            )
        )
    ]

    # Remove all tests where (ArmNN version is None or FastMath is None) and runtime is ArmNN
    test_params = [
        x
        for x in test_params
        if not (
            (x[TestParams.ARMNN_VERSION] == None or x[TestParams.FAST_MATH] == None)
            and (x[TestParams.TFLITE_RUNTIME] == "armnn")
        )
    ]

    logging.debug(test_params)

    result_set = defaultdict(lambda: {})
    result = None

    for param_set in test_params:
        if param_set[TestParams.BENCHMARK_TYPE] == "tflite":
            result = benchmark_tflite(param_set, result_set)
        elif param_set[TestParams.BENCHMARK_TYPE] == "triton":
            result = benchmark_triton(param_set, result_set)

        if args.output_file:
            add_row_to_csv_file(args.output_file, param_set, result)
