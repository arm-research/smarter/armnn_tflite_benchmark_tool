from benchmark import Benchmark
import re
import docker
import requests
import tflite_runtime.interpreter as tflite
from jinja2 import Template
import numpy as np
import pathlib
from collections import defaultdict
from time import sleep
import logging


class TritonBenchmark(Benchmark):
    def __init__(
        self,
        triton_image,
        perf_analyzer_image,
        runtime,
        cpu_instances=1,
        gpu_instances=0,
        *args,
        **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.triton_image = triton_image
        self.perf_analyzer_image = perf_analyzer_image
        self.runtime = runtime
        self.cpu_instances = cpu_instances
        self.gpu_instances = gpu_instances

        # Gather information about model using tflite python api
        interpreter = tflite.Interpreter(model_path=self.model_path)
        self.model_input_details = interpreter.get_input_details()
        self.model_output_details = interpreter.get_output_details()

        # Create triton datatype string type in input and output details
        input_shapes_missing = False
        for input_detail in self.model_input_details:
            input_detail["triton_datatype"] = self._to_triton_type_str(
                input_detail["dtype"]
            )
            # Check that input shapes are in model
            if len(input_detail["shape"]) == 0:
                input_shapes_missing = True

        # For some models the output shape isn't known until allocate tensors is called
        output_shapes_missing = False
        for output_detail in self.model_output_details:
            output_detail["triton_datatype"] = self._to_triton_type_str(
                output_detail["dtype"]
            )
            # Check that output shapes are in model
            if len(output_detail["shape"]) == 0:
                output_shapes_missing = True

        # For some models you have to find the shape of the input and output tensors after the allocation is done
        if input_shapes_missing or output_shapes_missing:
            interpreter.allocate_tensors()
            if input_shapes_missing:
                for input_detail in self.model_input_details:
                    input_detail["shape"] = interpreter.get_tensor(
                        int(input_detail["index"])
                    ).shape
            if output_shapes_missing:
                for output_detail in self.model_output_details:
                    output_detail["shape"] = interpreter.get_tensor(
                        int(output_detail["index"])
                    ).shape

    def run(self):
        inference_results = defaultdict(lambda: {})
        # Run default tflite test
        if self.runtime == "default":
            (
                inference_results["compute_latency"],
                inference_results["client_latency"],
                inference_results["throughput"],
            ) = self._benchmark_test()

        # Run tflite test with XNNPACK delegate
        elif self.runtime == "xnnpack":
            (
                inference_results["compute_latency"],
                inference_results["client_latency"],
                inference_results["throughput"],
            ) = self._benchmark_test(xnnpack=True)

        # Run tflite test with ArmNN delegate
        elif self.runtime == "armnn":
            (
                inference_results["compute_latency"],
                inference_results["client_latency"],
                inference_results["throughput"],
            ) = self._benchmark_test(armnn=True)

        # Inference latency returned as int in us
        return inference_results

    def _benchmark_test(self, armnn=False, xnnpack=False, protocol="grpc"):

        # Write model configuration using given parameters
        self._generate_template(armnn, xnnpack)

        # Remove old tritonserver container if still running
        try:
            tritonserver = self.client.containers.get("tritonserver")
            tritonserver.stop()
        except docker.errors.NotFound as e:
            pass

        try:
            # Start docker container with tflite model mounted in as volume
            volumes = [
                "{}/model-config:/models/test_model".format(
                    pathlib.Path(__file__).parent.resolve()
                ),
                "{}:/models/test_model/1/model.tflite".format(self.model_path),
            ]

            # Mount libmali if running on gpu
            devices = []
            if self.gpu_instances > 0:
                volumes.append(
                    "/usr/lib/aarch64-linux-gnu/libmali.so:/usr/lib/aarch64-linux-gnu/libmali.so"
                )
                devices = ["/dev/mali0:/dev/mali0:rwm"]
            triton_container = self.client.containers.run(
                image=self.triton_image,
                command=[
                    "/opt/tritonserver/bin/tritonserver",
                    "--model-repository",
                    "/models",
                ],
                name="tritonserver",
                volumes=volumes,
                ports={"8000/tcp": ("127.0.0.1", 8000)},
                detach=True,
                auto_remove=True,
                devices=devices,
            )
        except docker.errors.ContainerError as e:
            logging.info("Container run error: {}".format(e))
            return None, None, None
        except docker.errors.APIError as e:
            logging.info("API error when starting triton container: {}".format(e))
            return None, None, None
        except docker.errors.ImageNotFound as e:
            logging.info("Failed to find image: {}".format(self.triton_image))
            return None, None, None

        sleep(1)

        # First wait for model to be healthy
        while not (self._model_healthy()):
            sleep(2)

        # create perf_analyzer command string
        if protocol.lower() == "grpc":
            perf_analyzer_command = (
                "perf_client -i grpc -u localhost:8001 -m test_model"
            )
        elif protocol.lower() == "http":
            perf_analyzer_command = (
                "perf_client -i http -u localhost:8000 -m test_model"
            )

        try:
            # Create perf_analyzer container used to benchmark triton models
            # Container is created in same network namespace as triton container
            output = self.client.containers.run(
                image=self.perf_analyzer_image,
                command=perf_analyzer_command,
                name="perf_analyzer",
                network_mode="container:tritonserver",
                auto_remove=True,
            )

            server_compute_latency = self._parse_compute_inference_time(str(output))
            client_latency = self._parse_total_inference_time(str(output))
            thoughput = self._parse_inference_throughput(str(output))

        except docker.errors.ContainerError as e:
            logging.info("Container run error: {}".format(e))
            return None, None, None
        except docker.errors.APIError as e:
            logging.info(
                "API error when starting perf_analyzer container: {}".format(e)
            )
            return None, None, None
        except docker.errors.ImageNotFound as e:
            logging.info("Failed to find image: {}".format(self.perf_analyzer_image))
            return None, None, None

        finally:
            triton_container.stop()

        return (server_compute_latency, client_latency, thoughput)

    def _generate_template(self, armnn=False, xnnpack=False):
        with open("model-config/config-template.pbtxt") as file_:
            template = Template(
                file_.read(),
                trim_blocks=True,
                lstrip_blocks=True,
                keep_trailing_newline=True,
            )
        output_config = template.render(
            input_details=self.model_input_details,
            output_details=self.model_output_details,
            max_batch_size=0,  # TODO: Set this dynamically
            gpu=self.gpu_instances,
            cpu=self.cpu_instances,
            num_threads=self.num_threads,
            fast_math=self.use_fast_math,
            armnn=armnn,
            xnnpack=xnnpack,
        )
        logging.debug("Writing output config:\n{}".format(output_config))
        with open(
            "{}/model-config/config.pbtxt".format(
                pathlib.Path(__file__).parent.resolve()
            ),
            "w+",
        ) as output_file_:
            output_file_.write(output_config)

    def _model_healthy(self):
        try:
            response = requests.get("http://localhost:8000/v2/models/test_model/ready")
            if response.status_code == 200:
                return True
            else:
                return False
        except Exception as e:
            return False

    def _to_triton_type_str(self, np_cls):
        """Convert np datatype to triton type string for model config"""
        if np_cls is np.float64:
            return "TYPE_FP64"
        elif np_cls is np.float32:
            return "TYPE_FP32"
        elif np_cls is np.float16:
            return "TYPE_FP16"
        elif np_cls is np.int64:
            return "TYPE_INT64"
        elif np_cls is np.int32:
            return "TYPE_INT32"
        elif np_cls is np.int16:
            return "TYPE_INT16"
        elif np_cls is np.int8:
            return "TYPE_INT8"
        elif np_cls is np.uint64:
            return "TYPE_UINT64"
        elif np_cls is np.uint32:
            return "TYPE_UINT32"
        elif np_cls is np.uint16:
            return "TYPE_UINT16"
        elif np_cls is np.uint8:
            return "TYPE_UINT8"
        elif np_cls is np.byte:
            return "TYPE_BYTES"
        elif np_cls is np.bool_:
            return "TYPE_BOOL"
        else:
            return None

    def _parse_float_output(self, search_string, result):
        m = re.search(search_string, result)
        if m:
            return float(m.group(1))
        else:
            logging.debug("No match found with output {}".format(result))
            return None

    def _parse_compute_inference_time(self, result):
        return self._parse_float_output("compute infer (\d+\.\d+|\d+)", result)

    def _parse_total_inference_time(self, result):
        return self._parse_float_output("latency (\d+\.\d+|\d+)", result)

    def _parse_inference_throughput(self, result):
        return self._parse_float_output("Throughput: (\d+\.\d+|\d+)", result)
