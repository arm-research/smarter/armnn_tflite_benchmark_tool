from benchmark import Benchmark
import re
import docker
from collections import defaultdict
import logging


class TFLiteBenchmark(Benchmark):
    def __init__(
        self, armnn_tflite_image, tflite_version, runtime, use_gpu, *args, **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.armnn_tflite_image = armnn_tflite_image
        self.tflite_version = tflite_version
        self.runtime = runtime
        self.use_gpu = use_gpu

    def run(self):
        inference_results = defaultdict(lambda: {})

        # Run default tflite test
        if self.runtime == "default":
            (
                inference_results["compute_latency"],
                inference_results["memory"],
            ) = self._benchmark_test(
                "/benchmarking/benchmark_model --graph=/model.tflite --num_threads={}".format(
                    self.num_threads
                )
            )

        # Run tflite test with XNNPACK delegate
        elif self.runtime == "xnnpack":
            (
                inference_results["compute_latency"],
                inference_results["memory"],
            ) = self._benchmark_test(
                "/benchmarking/benchmark_model --graph=/model.tflite --num_threads={} --use_xnnpack=true".format(
                    self.num_threads
                )
            )

        # Run tflite test with ArmNN delegate
        elif self.runtime == "armnn" and not (self.use_gpu):
            (
                inference_results["compute_latency"],
                inference_results["memory"],
            ) = self._benchmark_test(
                '/benchmarking/benchmark_model --graph=/model.tflite --num_threads={} --external_delegate_path=/root/armnn-devenv/armnn/build/delegate/libarmnnDelegate.so --external_delegate_options="backends:CpuAcc;number-of-threads:{};enable-fast-math:{}"'.format(
                    self.num_threads,
                    self.num_threads,
                    "true" if self.use_fast_math else "false",
                )
            )

        elif self.runtime == "armnn" and self.use_gpu:
            (
                inference_results["compute_latency"],
                inference_results["memory"],
            ) = self._benchmark_test(
                '/benchmarking/benchmark_model --graph=/model.tflite --num_threads={} --external_delegate_path=/root/armnn-devenv/armnn/build/delegate/libarmnnDelegate.so --external_delegate_options="backends:GpuAcc;enable-fast-math:{}"'.format(
                    self.num_threads, "true" if self.use_fast_math else "false"
                )
            )

        # Inference latency returned as int in us
        return inference_results

    def _benchmark_test(self, command):
        # Start docker container with tflite model mounted in as volume
        logging.debug("Running command: {}".format(command))
        volumes = ["{}:/model.tflite".format(self.model_path)]
        devices = []
        if self.use_gpu:
            volumes.append(
                "/usr/lib/aarch64-linux-gnu/libmali.so:/usr/lib/aarch64-linux-gnu/libmali.so"
            )
            devices = ["/dev/mali0:/dev/mali0:rwm"]
        try:
            output = self.client.containers.run(
                image=self.armnn_tflite_image,
                command=command,
                volumes=volumes,
                auto_remove=True,
                devices=devices,
            )

            return self._parse_inference_time(str(output)), self._parse_memory_usage(
                str(output)
            )
        except docker.errors.ContainerError as e:
            logging.info("Container run error: {}".format(e))
            return None, None
        except docker.errors.APIError as e:
            logging.info("API error when starting armnn tflite container: {}".format(e))
            return None, None
        except docker.errors.ImageNotFound as e:
            logging.info("Failed to find image: {}".format(self.armnn_tflite_image))
            return None, None

    def _parse_inference_time(self, result):
        m = re.search("Inference \(avg\): (\d+\.\d+|\d+)(e\+(\d+)|)", result)
        if m:
            logging.debug("Match found with output {}".format(result))
            if m.group(3):
                return float(m.group(1)) * (10 ** int(m.group(3)))
            else:
                return float(m.group(1))
        else:
            logging.debug("No match found with output {}".format(result))
            return None

    def _parse_memory_usage(self, result):
        m = re.search("overall=(\d+\.\d+|\d+)(e\+(\d+)|)", result)
        if m:
            logging.debug("Match found with output {}".format(result))
            return float(m.group(1))
        else:
            logging.debug("No match found with output {}".format(result))
            return None
